@extends('layout.master')

@section('judul')
  Tambah Berita
@endsection

@section('content')
<form action="/berita" method="post" enctype="multipart/form-data">
  @csrf
  <div class="form-group">
    <label>Judul Berita</label>
    <input type="text" class="form-control" placeholder="Ketikkan Judul Berita" name="judul">
  </div>
  @error('judul') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <div class="form-group">
    <label>Isi Berita</label>
    <textarea class="form-control" name="isi"></textarea>
    @error('isi') 
    <div class="alert alert danger">{{$message}}</div>
    @enderror
  </div>
  <div class="form-group">
    <label>Kategori</label>
    <select class="form-control" name="kategori_id">
      <option value="">---Pilih Kategori---</option>
      @foreach ($kategori as $item)
          <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
      @endforeach
    </select>
    @error('kategori_id') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  </div>
  <div class="form-group">
    <label>Thumbnail</label>
    <input type="file" class="form-control" name="thumbnail">
  </div>
  @error('thumbnail') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <input type="submit" class="btn btn-primary" value="Simpan">
  <a href="/berita" class="btn btn-secondary">Kembali</a>
</form>
@endsection