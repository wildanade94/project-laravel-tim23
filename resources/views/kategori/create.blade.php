@extends('layout.master')

@section('judul')
  Tambah Kategori
@endsection

@section('content')
<form action="/kategori" method="post">
  @csrf
  <div class="form-group">
    <label>Nama Kategori</label>
    <input type="text" class="form-control" placeholder="Ketikkan Nama Kategori" name="nama_kategori">
  </div>
  @error('nama_kategori') 
  <div class="alert alert danger">{{$message}}</div>
  @enderror
  <input type="submit" class="btn btn-primary" value="Simpan">
  <a href="/kategori" class="btn btn-secondary">Kembali</a>
</form>
@endsection