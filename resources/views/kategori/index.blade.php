@extends('layout.master')

@section('judul')
    Data Kategori
@endsection

@section('content')
<div class="table-responsive">
  <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
      <thead>
          <tr>
              <th width="25px">No</th>
              <th width="100px">Nama Kategori</th>
              <th width="200px">Aksi</th>
          </tr>
      </thead>
      <tbody>
        <?php $no = 0; ?>
        @forelse ($kategori as $item)
            <tr>
                <td>{{$no = $no + 1}}</td>
                <td>{{$item->nama_kategori}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                    @csrf
                    @method('DELETE')
                    <a href="/kategori/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm" onClick="return confirm('Anda yakin ingin menghapus data : ?')">
                    </form>
                </td>
            </tr>      
        @empty
            <tr>
               <td><h4>Data kategori belum ada</h4></td>
            </tr> 
        @endforelse
          
      </tbody>
  </table>
@endsection